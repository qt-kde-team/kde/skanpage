skanpage (24.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (24.12.0).
  * Update build-deps and deps with the info from cmake.
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 13 Dec 2024 11:23:17 +0100

skanpage (24.08.2-1) experimental; urgency=medium

  [ Jesse Rhodes ]
  * New upstream release (24.05.2).
  * Update build-deps with info from cmake.

  [ Aurélien COUDERC ]
  * New upstream release (24.08.2).
  * Update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.7.0, no change required.
  * Switch to declarative KF6 debhelper build sequence.
  * Replace manual QML runtime dependencies with dh_qmldeps.
  * Review copyright information.
  * Slightly simplify debian/rules with the use of the
    before_dh_auto_clean target.
  * Add/update Heiko Becker’s key in upstream keyring.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 16 Oct 2024 09:34:30 +0200

skanpage (23.08.1-2.1) unstable; urgency=medium

  * Non-maintainer upload
  * Leptonlib transition, bug #1081319

 -- Jeff Breidenbach <jab@debian.org>  Sat, 05 Oct 2024 14:13:00 -0700

skanpage (23.08.1-2) unstable; urgency=medium

  * Team upload.

  [ Jesse Rhodes ]
  * Add missing runtime dependency on qml-module-org-kde-
    kquickimageeditor. (Closes: #1053406)

 -- Jesse Rhodes <jesse@sney.ca>  Fri, 06 Oct 2023 10:30:59 -0400

skanpage (23.08.1-1) unstable; urgency=medium

  * Team upload.

  [ Aurélien COUDERC ]
  * Add missing runtime dependency on qml-module-org-kde-purpose. (Closes:
    #1030337)

  [ Jesse Rhodes ]
  * New upstream release (23.08.1).
  * Update build-deps with info from cmake.
  * Update debian/rules to remove a build artifact.

 -- Jesse Rhodes <jesse@sney.ca>  Mon, 02 Oct 2023 17:22:41 -0400

skanpage (22.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.3).
  * Update build-deps and deps with the info from cmake.
  * Added myself to the uploaders.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 01 Mar 2023 11:58:27 +0100

skanpage (22.12.1-2) unstable; urgency=medium

  [ Scarlett Moore ]
  * Source only upload.

 -- Scarlett Moore <sgmoore@debian.org>  Fri, 03 Feb 2023 06:17:29 -0700

skanpage (22.12.1-1) unstable; urgency=medium

  * Initial release (Closes: #998815)

 -- Scarlett Moore <sgmoore@debian.org>  Thu, 19 Jan 2023 06:17:56 -0700
